/**
 * Gets the DICOM headers associated with the given session url's. 
 * Headers are retrieved serially so the user can control when the process can stop.
 */
qx.Class.define("dicomheaderview.GetHeaders",
{
  extend : qx.core.Object,
  /**
   * @param serverRoot{String} The root url for this XNAT server
   * @param urls{[String]} The session url's for which to retrieve headers
   * @param incrementNotifier{Function} Function to run after each url has been retrieved
   * @param cancelAction{Function} Function to stop the retrieval of headers
   */
  construct : function (serverRoot, urls, incrementNotifier, cancelAction) {
    this.base(arguments);
    this.initCounter(urls.length);
    this.initUrls(urls);
    this.initServerRoot(serverRoot);
    this.initHeaders([]);
    this.initIncrementNotifier(incrementNotifier);
    var that = this;
    this.initCancelAction(cancelAction(function () {that.debug("Stopping"); that.setStop(true);}));
  },
  events : {
    /**
     * Event fired when all the headers have been retrieved. 
     * Carries the 'header' property
     */
    "completed" : "qx.event.type.Data"
  },
  properties : {
    /**
     * Function to run if a user cancels downloading the headers
     */
    cancelAction : {
      deferredInit : true
    },
    /**
     * The name of the XNAT server
     */
    serverRoot : {
      deferredInit : true
    },
    /**
     * A list of session url's passed in by the user
     */
    urls : {
      deferredInit : true
    },
    /**
     * TODO doesn't appear to be used. Verify and update docs
     */
    counter : {
      deferredInit : true
    },
    /**
     * A list of DICOM headers as they are retrieved from the server
     */
    headers : {
      deferredInit : true
    },
    /**
     * Notify the user as each header is returned from the server.
     */
    incrementNotifier : {
      deferredInit : true,
      nullable : true
    },
    /**
     * A flag that determines whether to stop retrieving headers
     */
    stop : {
      init : false,
      check : "Boolean"
    }
  },
  members : {
    /**
     * Retrieve headers serially using the list of urls passed in by the user.
     */
    retrieveHeaders : function () {
      var that = this;
      var curr = 0;

      var createRequest = function () {
	return new qx.io.remote.Request(that.createHeaderUrl(that.getServerRoot(),
							       that.getUrls()[curr]),
					  "GET");

      };

      /**
       * Each time a header is retrieved this function is called to either
       * retrieve the next header or stop.
       */
      var next = function (){
	if (that.getStop() === false) {
	  curr++;
	  if (curr < that.getUrls().length) {
	    var req = createRequest();
	    getNextHeader(req);
	  }
	  else {
	    that.fireDataEvent("completed", {headers : that.getHeaders()});
	  }
	}
	else {
	  that.debug("Stopped");
	}
      };

      /**
       * Notify the user after each header is retrieved
       */
      var notify = function () {
	if (that.getIncrementNotifier() !== null) {
	  that.getIncrementNotifier()(1);
	}
      };

      /**
       * Create a request for the next header and decorate it with 
       * the necessary event listeners. 
       */
      var getNextHeader = function (req) {
	var keepGoing = function (e, success) {
	  if (!success){
	    notify();
	    next();
	  }
	  else {
	    notify();
	    that.addHeader(that.getUrls()[curr], e.getContent());
	    next();  
	  }
	};
	req.addListener("completed", function(e) {
			  keepGoing(e, true);
			}, that);
	req.addListener("failed", function(e) {
			  keepGoing(e,false);
			},that);
	req.addListener("timeout", function(e) {
			  keepGoing(e,false);
			},that);
	req.addListener("aborted", function(e) {
			  keepGoing(e,false);
			},that);
	req.send();
      };

      if (this.getUrls().length != 0) {
	getNextHeader(createRequest());
      }
      else {
	this.getCancelAction()();
	this.fireDataEvent("completed", {headers : this.getHeaders()});
      }
    },

    /**
     * Marshall the header and add it to the global list of headers
     * @param curr{Integer} The index of the header
     * @param data{Object} A CSV representation of the headers 
     */
    addHeader : function (curr, data) {
      var expData = new csvparser.CSVParser().csvToArray(data);
      var expObj = dicomheaderview.ExperimentTable.toObject(expData);
      this.getHeaders().push({index : curr, data : expObj});
    },

    /**
     * Create the server request URL to retrieve the headers
     * @param serverRoot{String} The root of the XNAT server
     * @param srcUrl{String} The url for the session 
     */
    createHeaderUrl : function (serverRoot, srcUrl) {
      var url = serverRoot + "/REST/services/dicomdump?src=" + srcUrl + "&format=csv";
      return url;
    }
  }
});
