/* ************************************************************************

@asset(dicomheaderview/*)

************************************************************************ */

/**
 * This is the main application class of your custom application "dicomheaderview"
 */
qx.Class.define("dicomheaderview.Application",
{
  extend : qx.application.Inline,
  members : {
    main : function() {
      this.base(arguments);
      if (qx.core.Environment.get('qx.debug') == "on") {
        qx.log.appender.Native;
        qx.log.appender.Console;
      }
      var numdays = document.getElementById("numdays").attributes["num"].value;
      var rootContainer = new qx.ui.root.Inline(document.getElementById("archiveheader"));
      rootContainer.setZIndex(0);
      var serverRoot = document.getElementById("serverRoot").title;
      var dummy = {
	open : function (c) {},
	close : function (c) {}
      };
      new dicomheaderview.DicomHeaderView(serverRoot, rootContainer, dummy, dummy, "archive", parseInt(numdays));
    }
  }
});
