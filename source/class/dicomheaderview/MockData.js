qx.Class.define("dicomheaderview.MockData", 
{
  type : "static",
  statics : {
    experiments : function (numExps) {
      var ret = [];
      for (var i = 0; i < numExps; i++) {
	ret.push("exp" + i);
      }
      return ret;
    },
    csv : "\"tag1\",\"tag2\",\"vr\",\"value\",\"desc\" \n\
\"(0002,0001)\",\"\",\"OB\",\"00\01\",\"File Meta Information Version\" \n\
\"(0002,0002)\",\"\",\"UI\",\"1.2.840.10008.5.1.4.1.1.4.1\",\"Media Storage SOP Class UID\" \n\
\"(0002,0003)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.1.1.5020.1.1.1166546115.14677\",\"Media Storage SOP Instance UID\" \n\
\"(0002,0010)\",\"\",\"UI\",\"1.2.840.10008.1.2.1\",\"Transfer Syntax UID\" \n\
\"(0002,0012)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.2\",\"Implementation Class UID\" \n\
\"(0002,0013)\",\"\",\"SH\",\"DCTOOL100\",\"Implementation Version Name\" \n\
\"(0002,0016)\",\"\",\"AE\",\"CLUNIE1\",\"Source Application Entity Title\" \n\
\"(0008,0005)\",\"\",\"CS\",\"ISO_IR 100\",\"Specific Character Set\" \n\
\"(0008,0008)\",\"\",\"CS\",\"ORIGINAL\PRIMARY\T1\NONE\",\"Image Type\" \n\
\"(0008,0012)\",\"\",\"DA\",\"20061219\",\"Instance Creation Date\" \n\
\"(0008,0013)\",\"\",\"TM\",\"150932\",\"Instance Creation Time\" \n\
\"(0008,0014)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.3\",\"Instance Creator UID\" \n\
\"(0008,0016)\",\"\",\"UI\",\"1.2.840.10008.5.1.4.1.1.4.1\",\"SOP Class UID\" \n\
\"(0008,0018)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.1.1.5020.1.1.1166546115.14677\",\"SOP Instance UID\" \n\
\"(0008,0020)\",\"\",\"DA\",\"20061219\",\"Study Date\" \n\
\"(0008,0021)\",\"\",\"DA\",\"20061219\",\"Series Date\" \n\
\"(0008,0023)\",\"\",\"DA\",\"20061219\",\"Content Date\" \n\
\"(0008,002A)\",\"\",\"DT\",\"20061219083214\",\"Acquisition DateTime\" \n\
\"(0008,0030)\",\"\",\"TM\",\"083214\",\"Study Time\" \n\
\"(0008,0031)\",\"\",\"TM\",\"083214\",\"Series Time\" \n\
\"(0008,0033)\",\"\",\"TM\",\"083214\",\"Content Time\" \n\
\"(0008,0050)\",\"\",\"SH\",\"9995020\",\"Accession Number\" \n\
\"(0008,0060)\",\"\",\"CS\",\"MR\",\"Modality\" \n\
\"(0008,0070)\",\"\",\"LO\",\"Acme Medical Devices\",\"Manufacturer\" \n\
\"(0008,0080)\",\"\",\"LO\",\"St. Nowhere Hospital\",\"Institution Name\" \n\
\"(0008,0090)\",\"\",\"PN\",\"Thomas^Albert\",\"Referring Physician's Name\" \n\
\"(0008,0201)\",\"\",\"SH\",\"-0500\",\"Timezone Offset From UTC\" \n\
\"(0008,1010)\",\"\",\"SH\",\"CONSOLE01\",\"Station Name\" \n\
\"(0008,1030)\",\"\",\"LO\",,\"Study Description\" \n\
\"(0008,103E)\",\"\",\"LO\",\"* SAG LOC\",\"Series Description\" \n\
\"(0008,1050)\",\"\",\"PN\",\"Smith^John\",\"Performing Physician's Name\" \n\
\"(0008,1060)\",\"\",\"PN\",\"Smith^John\",\"Name of Physician(s) Reading Study\" \n\
\"(0008,1070)\",\"\",\"PN\",\"Jones^Molly\",\"Operators' Name\" \n\
\"(0008,1090)\",\"\",\"LO\",\"Super Dooper Scanner\",\"Manufacturer's Model Name\" \n\
\"(0008,9121)\",\"\",\"SQ\",\"\",\"Referenced Raw Data Sequence\" \n\
\"(0008,9121)\",\"(0008,1115)\",\"SQ\",\"\",\"Referenced Series Sequence\" \n\
\"(0008,9121)\",\"(0020,000D)\",\"UI\",\"1.3.6.1.4.1.5962.1.2.5020.1166546115.14677\",\"Study Instance UID\" \n\
\"(0008,9205)\",\"\",\"CS\",\"MONOCHROME\",\"Pixel Presentation\" \n\
\"(0008,9206)\",\"\",\"CS\",\"VOLUME\",\"Volumetric Properties\" \n\
\"(0008,9207)\",\"\",\"CS\",\"NONE\",\"Volume Based Calculation Technique\" \n\
\"(0008,9208)\",\"\",\"CS\",\"MAGNITUDE\",\"Complex Image Component\" \n\
\"(0008,9209)\",\"\",\"CS\",\"T1\",\"Acquisition Contrast\" \n\
\"(0010,0010)\",\"\",\"PN\",\"Subject_001\",\"Patient's Name\" \n\
\"(0010,0020)\",\"\",\"LO\",,\"Patient ID\" \n\
\"(0010,0030)\",\"\",\"DA\",\"19500704\",\"Patient's Birth Date\" \n\
\"(0010,0040)\",\"\",\"CS\",\"M\",\"Patient's Sex\" \n\
\"(0010,1010)\",\"\",\"AS\",\"052Y\",\"Patient's Age\" \n\
\"(0010,1020)\",\"\",\"DS\",\"1.6\",\"Patient's Size\" \n\
\"(0010,1030)\",\"\",\"DS\",\"75\",\"Patient's Weight\" \n\
\"(0010,21B0)\",\"\",\"LT\",,\"Additional Patient History\" \n\
\"(0012,0064)\",\"\",\"SQ\",\"\",\"De-identification Method Code Sequence\" \n\
\"(0012,0064)\",\"(0008,0100)\",\"SH\",\"655374\",\"Code Value\" \n\
\"(0012,0064)\",\"(0008,0102)\",\"SH\",\"XNAT\",\"Coding Scheme Designator\" \n\
\"(0012,0064)\",\"(0008,0103)\",\"SH\",\"0.1\",\"Coding Scheme Version\" \n\
\"(0012,0064)\",\"(0008,0104)\",\"LO\",\"XNAT Edit Script\",\"Code Meaning\" \n\
\"(0018,0023)\",\"\",\"CS\",\"2D\",\"MR Acquisition Type\" \n\
\"(0018,0087)\",\"\",\"DS\",\"1.5\",\"Magnetic Field Strength\" \n\
\"(0018,1000)\",\"\",\"LO\",\"123456\",\"Device Serial Number\" \n\
\"(0018,1020)\",\"\",\"LO\",\"1.00\",\"Software Version(s)\" \n\
\"(0018,1050)\",\"\",\"DS\",\"1.093750\",\"Spatial Resolution\" \n\
\"(0018,5100)\",\"\",\"CS\",\"HFS\",\"Patient Position\" \n\
\"(0018,9004)\",\"\",\"CS\",\"PRODUCT\",\"Content Qualification\" \n\
\"(0018,9005)\",\"\",\"SH\",\"UNNAMED\",\"Pulse Sequence Name\" \n\
\"(0018,9008)\",\"\",\"CS\",\"SPIN\",\"Echo Pulse Sequence\" \n\
\"(0018,9011)\",\"\",\"CS\",\"NO\",\"Multiple Spin Echo\" \n\
\"(0018,9012)\",\"\",\"CS\",\"YES\",\"Multi-planar Excitation\" \n\
\"(0018,9014)\",\"\",\"CS\",\"NO\",\"Phase Contrast\" \n\
\"(0018,9015)\",\"\",\"CS\",\"NO\",\"Time of Flight Contrast\" \n\
\"(0018,9017)\",\"\",\"CS\",\"NONE\",\"Steady State Pulse Sequence\" \n\
\"(0018,9018)\",\"\",\"CS\",\"NO\",\"Echo Planar Pulse Sequence\" \n\
\"(0018,9024)\",\"\",\"CS\",\"NO\",\"Saturation Recovery\" \n\
\"(0018,9025)\",\"\",\"CS\",\"NONE\",\"Spectrally Selected Suppression\" \n\
\"(0018,9029)\",\"\",\"CS\",\"NONE\",\"Oversampling Phase\" \n\
\"(0018,9032)\",\"\",\"CS\",\"RECTILINEAR\",\"Geometry of k-Space Traversal\" \n\
\"(0018,9033)\",\"\",\"CS\",\"SINGLE\",\"Segmented k-Space Traversal\" \n\
\"(0018,9034)\",\"\",\"CS\",\"LINEAR\",\"Rectilinear Phase Encode Reordering\" \n\
\"(0018,9064)\",\"\",\"CS\",\"NONE\",\"k-space Filtering\" \n\
\"(0018,9073)\",\"\",\"FD\",\"51.20001220703124\",\"Acquisition Duration\" \n\
\"(0018,9093)\",\"\",\"US\",\"1\",\"Number of k-Space Trajectories\" \n\
\"(0018,9100)\",\"\",\"CS\",\"1H\",\"Resonant Nucleus\" \n\
\"(0018,9174)\",\"\",\"CS\",\"FDA\",\"Applicable Safety Standard Agency\" \n\
\"(0020,000D)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.1.2.5020.1166546115.14677\",\"Study Instance UID\" \n\
\"(0020,000E)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.1.3.5020.1.1166546115.14677\",\"Series Instance UID\" \n\
\"(0020,0010)\",\"\",\"SH\",\"05020\",\"Study ID\" \n\
\"(0020,0011)\",\"\",\"IS\",\"1\",\"Series Number\" \n\
\"(0020,0012)\",\"\",\"IS\",\"1\",\"Acquisition Number\" \n\
\"(0020,0013)\",\"\",\"IS\",\"1\",\"Instance Number\" \n\
\"(0020,0052)\",\"\",\"UI\",\"1.3.6.1.4.1.5962.1.4.5020.1.1166546115.14677\",\"Frame of Reference UID\" \n\
\"(0020,1040)\",\"\",\"LO\",\"NA\",\"Position Reference Indicator\" \n\
\"(0028,0002)\",\"\",\"US\",\"1\",\"Samples per Pixel\" \n\
\"(0028,0004)\",\"\",\"CS\",\"MONOCHROME2\",\"Photometric Interpretation\" \n\
\"(0028,0008)\",\"\",\"IS\",\"15\",\"Number of Frames\" \n\
\"(0028,0010)\",\"\",\"US\",\"256\",\"Rows\" \n\
\"(0028,0011)\",\"\",\"US\",\"256\",\"Columns\" \n\
\"(0028,0100)\",\"\",\"US\",\"16\",\"Bits Allocated\" \n\
\"(0028,0101)\",\"\",\"US\",\"16\",\"Bits Stored\" \n\
\"(0028,0102)\",\"\",\"US\",\"15\",\"High Bit\" \n\
\"(0028,0103)\",\"\",\"US\",\"1\",\"Pixel Representation\" \n\
\"(0028,0301)\",\"\",\"CS\",\"NO\",\"Burned In Annotation\" \n\
\"(0028,2110)\",\"\",\"CS\",\"00\",\"Lossy Image Compression\" \n\
\"(2050,0020)\",\"\",\"CS\",\"IDENTITY\",\"Presentation LUT Shape\" \n\
\"(5200,9229)\",\"(5200,9229)\",\"SQ\",\"\",\"Shared Functional Groups Sequence\" \n\
\"(5200,9229)\",\"(0018,9006)\",\"SQ\",\"\",\"MR Imaging Modifier Sequence\" \n\
\"(5200,9229)\",\"(0018,9042)\",\"SQ\",\"\",\"MR Receive Coil Sequence\" \n\
\"(5200,9229)\",\"(0018,9049)\",\"SQ\",\"\",\"MR Transmit Coil Sequence\" \n\
\"(5200,9229)\",\"(0018,9112)\",\"SQ\",\"\",\"MR Timing and Related Parameters Sequence\" \n\
\"(5200,9229)\",\"(0018,9114)\",\"SQ\",\"\",\"MR Echo Sequence\" \n\
\"(5200,9229)\",\"(0018,9115)\",\"SQ\",\"\",\"MR Modifier Sequence\" \n\
\"(5200,9229)\",\"(0018,9119)\",\"SQ\",\"\",\"MR Averages Sequence\" \n\
\"(5200,9229)\",\"(0018,9125)\",\"SQ\",\"\",\"MR FOV/Geometry Sequence\" \n\
\"(5200,9229)\",\"(0018,9226)\",\"SQ\",\"\",\"MR Image Frame Type Sequence\" \n\
\"(5200,9229)\",\"(0020,9071)\",\"SQ\",\"\",\"Frame Anatomy Sequence\" \n\
\"(5200,9229)\",\"(0020,9116)\",\"SQ\",\"\",\"Plane Orientation Sequence\" \n\
\"(5200,9229)\",\"(0028,9110)\",\"SQ\",\"\",\"Pixel Measures Sequence\" \n\
\"(5200,9229)\",\"(0028,9145)\",\"SQ\",\"\",\"Pixel Value Transformation Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\" \n\
\"(5200,9230)\",\"(5200,9230)\",\"SQ\",\"\",\"Per-frame Functional Groups Sequence\" \n\
\"(5200,9230)\",\"(0020,9111)\",\"SQ\",\"\",\"Frame Content Sequence\" \n\
\"(5200,9230)\",\"(0020,9113)\",\"SQ\",\"\",\"Plane Position Sequence\" \n\
\"(5200,9230)\",\"(0028,9132)\",\"SQ\",\"\",\"Frame VOI LUT Sequence\""
  }
});
