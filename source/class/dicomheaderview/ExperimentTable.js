/**
 * The model for the table that displays the dicom tags
 * 
 */
qx.Class.define("dicomheaderview.ExperimentTable",
{
  extend : qx.core.Object,
  construct : function (experiments, defaultVisibleTags) {
    this.base(arguments);
    this.setModel(this.initModel(experiments));
  },
  properties : {
    /**
     * The table model 
     */
    model : {
      deferredInit : true
    }
  },
  statics : {
    /**
     * Unquote a string
     * @param str{String} 
     */
    unQuote : function (str) {
      var ret = str;
      if (qx.lang.String.startsWith(str,"\"") && qx.lang.String.endsWith(str,"\"")){
	ret = str.substring(1, str.length -1);
	}
	return ret;
    },
    /**
     * Turn a parsed CSV table into an array of objects
     * Example input:
     * [["Col1", "Col2" "Col3"], 
     *  ['"a"', '"b"', '"c"'], 
     *  ['"d"', '"e"', '"f"']]
     * Example Output:
     * [
     *   { "Col1": "a", "Col2": "b", "Col3": "c" },
     *   { "Col1": "d", "Col2": "e", "Col3": "f" }
     * ]
     * @param lines{[[String]]} Each row of the CSV output
     */
    toObject : function (lines) {
      // Build the column names map from the first line.
      var colName = {};
      if (lines[0]) {
	for (var j = 0; j < lines[0].length; j++) {
	  if (lines[0][j] != undefined) {
	    colName[j.toString()] = dicomheaderview.ExperimentTable.unQuote(lines[0][j]);
	  }
	}
      }

      var ret = [];
      for (var i = 1; i < lines.length ; i++) {
	var line = lines[i];
	var obj = {};
	for (var j = 0; j < line.length; j++) {
	  if (line[j] != undefined) {
	    obj[colName[j]] = dicomheaderview.ExperimentTable.unQuote(line[j]);
	  }
	}
	ret.push(obj);
      }
      return ret;
    }
  },
  members : {
    /**
     * Create the model for the table that displays the headers as columns and their values as rows
     * @param experiments {Object} {@link dicomheaderview.GetHeaders#headers}
     */
    initModel : function (experiments) {
      var model = new qx.ui.table.model.Simple();
      var columns = [[],[]];
      columns[0].push("Name");
      columns[1].push("Name");
      var rows = [];
      var tags = {};
      
      for (var i = 0; i < experiments.length; i++) {
	for (var j = 0; j < experiments[i].data.length; j++) {
	  var _tag1 = experiments[i].data[j].tag1;
	  var _tag2 = experiments[i].data[j].tag2;
	  var _desc = experiments[i].data[j].desc;
	  var _vr = experiments[i].data[j].vr;
	  var current_tag = {};
	  if (!(_tag1 in tags)) {
	    tags[_tag1] = {};
	  }
	  if (_tag2 === "") {
	    tags[_tag1].type = "single";
	    current_tag = tags[_tag1];
	  }
	  else {
	    tags[_tag1].type = "nested";
	    tags[_tag1][experiments[i].name] = true;
	    if (!(_tag2 in tags[_tag1])) {
	      tags[_tag1][_tag2] = {};
	    }
	    current_tag = tags[_tag1][_tag2];
	    if (_tag1 === _tag2) {
	      tags[_tag1].vr = _vr;
	      tags[_tag1].desc = _desc;
	    }
	  }
	  current_tag.vr = _vr;
	  current_tag[experiments[i].name] = experiments[i].data[j].value;
	  current_tag.desc = _desc;
	}
      }

      for (var tag in tags) {
	columns[0].push(tags[tag].desc);
	columns[1].push(tag);
	for (var i = 0; i < experiments.length; i++) {
	  var name = experiments[i].name;
	  if (rows[i] === undefined) {
	    rows[i] = [];
	    rows[i].push(name);
	  }
	  if ((name in tags[tag]) && tags[tag].type === "single" ) {
	    rows[i].push(tags[tag][name]);
	  }
	  else if ((name in tags[tag]) && tags[tag].type === "nested") {
	    rows[i].push("<nested>");
	  }
	  else if (!(name in tags[tag])) {
	    rows[i].push("N/A");
	  }
	}
      }

      model.setColumns(columns[0], columns[1]);
      model.setData(rows);
      return model;
    },

    /**
     * Create the table showing only the default visible tags.
     * @param defaultVisibleTags{@link dicomtag.Tags#defaultVisibleTags}
     */
    createTable : function (defaultVisibleTags) {
      var model = this.getModel();
      var tableTags = this.getTableTags();
      var invisibleTags = tableTags.filter(function(t) {
					     return !(defaultVisibleTags.indexOf(t) > -1);
					   });
      
      var invisibleColumns = invisibleTags.filter(function(t) {
			       return !(t === "Name");
			       }).map(function(t) {
					return model.getColumnIndexById(t);
				      });
      var table = new qx.ui.table.Table(model, {
					  initiallyHiddenColumns : invisibleColumns
					});
      table.setMetaColumnCounts([1,-1]);
      table.setColumnVisibilityButtonVisible(false);
      return table;
    },

    /**
     * Unused ...
     */
    setVisibleTags : function (model, columnModel, visibleTags) {
      var columnCount = model.getColumnCount();
      for (var i = 0; i < columnCount; i++) {
	var tag = model.getColumnId(i);
	if (! visibleTags.indexOf(tag) > -1) {
	  var id = model.getColumnIndexById(tag);
	  columnModel.setColumnVisible(id,false);
	}
      }
    },

    /** 
     * Get the header fields shown by this table
     */
    getTableTags : function () {
      var model = this.getModel();
      var columnCount = model.getColumnCount();
      var ret = [];
      for (var i = 0; i < columnCount; i++) {
	ret.push(model.getColumnId(i));
      }
      return ret;
    },
      
    /**
     * Set the visibility of a header field.
     * @param table{Object} The header table
     * @param tag{String} The Dicom tag
     * @param visible{Boolean} If true, show this header field 
     */
      
    setVisibility : function (table, tag, visible) {
      var model = this.getModel();
      var columnId = model.getColumnIndexById(tag);
      var columnModel = table.getTableColumnModel();
      columnModel.setColumnVisible(columnId, visible);
    },

    /**
     * Move to the column that shows the given DICOM tag
     * @param table{Object} The header table
     * @param tag{String} DICOM tag
     */
    scrollToColumn : function (table,tag) {
      var model = this.getModel();
      var columnId = model.getColumnIndexById(tag);
      table.scrollCellVisible(columnId, 0);
    }
  }
});
