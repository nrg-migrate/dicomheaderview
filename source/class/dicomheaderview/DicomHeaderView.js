/**
 * Manage retrieving the recent sessions from XNAT, the headers for each of those sessions and drawing the session table and tag table in a container.
 * 
 * "project","timestamp","lastmod","uploaded","scan_date","scan_time","subject","folderName","name","tag","status","url","autoarchive"
 */

qx.Class.define("dicomheaderview.DicomHeaderView",
{
  extend : qx.core.Object,
  /**
   * See properties documentation for an explanation on the constructor arguments
   */
  construct : function (serverRoot, container, openAction, closeAction, archiveType, numDays) {
    this.base(arguments);
    this.initServerRoot(serverRoot);
    this.initArchiveType(archiveType);
    this.initNumDays(numDays);
    this.initOpenAction(openAction);
    this.initCloseAction(closeAction);
    this.initContainer(container);
    this.initProgressBar(new qx.ui.indicator.ProgressBar(0));
	this.main();
  },
  properties : {
    /**
     * Container in which to display the archive headers
     */
    container : {
      deferredInit : true
    },
    /**
     * Function that renders the headers in the 'container'  
     */
    openAction : {
      deferredInit : true
    },
    /**
     * Function that manages closing the container
     */
    closeAction : {
      deferredInit : true
    },
    /**
     * Root of the XNAT server
     */
    serverRoot : {
      deferredInit : true
    },
    /**
     * Either "archive" or "prearchive"
     */
    archiveType : {
      deferredInit : true
    },
    /**
     * Number of days worth of headers to retrieve
     */
    numDays : {
      check : "Integer",
      deferredInit : true
    },
    /**
     * As the headers are retrieved notify the user with this progress bar
     */
    progressBar : {
      check : "qx.ui.indicator.ProgressBar",
      deferredInit : true
    }
  },
  members : {
    main : function () {
      var that = this;
      var serverRoot = this.getServerRoot();
      var archiveType = this.getArchiveType();
      var numDays = this.getNumDays();
      var progressBar = this.getProgressBar();
      /** 
       * As each header is received update the progress bar
       */
      var incrementNotifier = function (x) {
	var curr = progressBar.getValue();
	progressBar.setValue(curr + x);
      };

      this.getContainer().add(progressBar);
      this.getOpenAction().open(this.getContainer());
      /**
       * Cancel header retrieval
       */
      var cancelAction = function (func) {
	if (that.getCloseAction().event !== undefined) {
	  that.getContainer().addListener(that.getCloseAction().event, function (evt) {
					    func();
					  });
	}
	else {
	  return func;
	}
      };

      var recentUrl = this.createRecentUrl(serverRoot,archiveType,numDays);
      /**
       * The request that retrieves the recent sessions
       */
      var recentReq = new qx.io.remote.Request(recentUrl, "GET");
      /**
       * Once the list of sessions has been retrieved get and render the DICOM headers
       * for each of the them.
       */
      recentReq.addListener("completed", function(evt) {
			      var data = evt.getContent();
			      var expData = new csvparser.CSVParser().csvToArray(data);
			      var objs = dicomheaderview.ExperimentTable.toObject(expData);
			      // create the urls for retrieving the DICOM header
			      var urls;
			      if (that.getArchiveType() === "archive") {
				urls = objs.map(function(o) {
						  return "/archive/projects/" + o.project + "/experiments/" + o.id;
						});

			      }
			      else {
				urls = objs.map(function(o) {
						    return o.url;
						  });
			      }
			      // Store the ids of the sessions
			      var names;
			      if (that.getArchiveType() === "archive") {
				names = objs.map(function(o) {
						     return o.id;
						   });
			      }
			      else {
				names = objs.map(function(o) {
						     return o.name;
						   });
			      }
					
					
				  if(objs.length>0){
			      progressBar.setMaximum(urls.length);
			      // retrieve the headers
			      var getHeaders = new dicomheaderview.GetHeaders(that.getServerRoot(),
									      urls,
									      incrementNotifier,
									      cancelAction
									      );
			      // Once retrieved render the table
			      getHeaders.addListener("completed", function(evt) {
						       var data = evt.getData();
						       that.getContainer().remove(progressBar);
						       var expsWithNames = [];
						       for (var h = 0; h < data.headers.length ; h++) {
							 expsWithNames.push({name : names[h],
									     data : data.headers[h].data});
						       }
						       var expTable = new dicomheaderview.ExperimentTable(expsWithNames,null);
						       var table = that.fillMainContainer(expTable);
						       that.getContainer().add(table);
						       that.getOpenAction().open(that.getContainer());
						     });
			      getHeaders.retrieveHeaders();
				  }
				  else{
				  that.getContainer().remove(progressBar);
				  var label1 = new qx.ui.basic.Label("DICOM Headers table cannot load because there are no sessions to view.").set({
					rich : true,
					width: 300
				  });
				  that.getContainer().add(label1);
				  }
				  
			    });
      recentReq.send();
    },

    /**
     * Create the url to retrieve the headers
     * @param serverRoot{String} The root of the XNAT server
     * @param srcUrl{String} The url of the session
     */
    createHeaderUrl : function (serverRoot, srcUrl) {
      var url = serverRoot + "/REST/services/dicomdump?src=" + srcUrl;
      return url;
    },
    /**
     * Create the url to retrieve the list of recent sessions
     * @param serverRoot{String} The root of the XNAT server
     * @param archiveType{String} "archive|prearchive"
     * @param numDays{Integer} How far to go back
     */
    createRecentUrl : function (serverRoot, archiveType, numDays) {
      var arc;
      if (archiveType === "archive") {
	arc = "experiments";
      }
      else {
	arc = "prearchive";
      }
      var url = serverRoot + "/REST/" + arc + "?recent=" + numDays + "&format=csv";
      return url;
    },
    /**
     * Used for testing, unused in production
     */
    createMockExperiments : function (expData) {
      var ret = [];
      var exps = dicomheaderview.MockData.experiments(100);
      for (var i = 0; i < exps.length; i++) {
      ret.push ({
		  name : exps[i],
		  data : expData
		});
      }
      return ret;
    },

    /**
     * Add the master list of tags and the table of sessions to the container.
     * Scroll the corresponding tag column in the session table if a tag is selected in the master list
     * Hide a column in the session table if the user unchecks it in the master list
     * @param experimentTable{Object} The table of experiments
     */
    fillMainContainer : function (experimentTable) {
      var container = new qx.ui.container.Composite(new qx.ui.layout.HBox());
      var t = new dicomtag.TagTable();
      var expTable = experimentTable.createTable(dicomtag.Tags.defaultVisibleTags);
      t.addListener("selectTag", function(evt){
		      var data = evt.getData();
		      experimentTable.scrollToColumn(expTable, data.tag);
		    });
      t.addListener("tagToggled", function(evt){
		      var data = evt.getData();
		      experimentTable.setVisibility(expTable, data.tag, data.visible);
		      experimentTable.scrollToColumn(expTable, data.tag);
		    });
      var experimentTableTags = experimentTable.getTableTags();
      var tagTableModel = t.createModel(experimentTableTags.slice(1));
      var tagTable = t.createPublicTable(tagTableModel);
      container.setWidth(900);
      container.add(tagTable);
      container.add(expTable, {flex : 1});
      return container;
    },

    /**
     * Retrieve the most recent sessions from the prearchive. Unused
     */
    serverRequest : function () {
      var url = "http://localhost:8080/xnat/REST/prearchive/experiments?recent=true";
      var req = new qx.io.remote.Request(url,"GET");
      req.setCrossDomain(true);
      return req;
    }

	  }
});
