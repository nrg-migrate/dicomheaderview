/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("dicomheaderview.theme.Theme",
{
  meta :
  {
    color : dicomheaderview.theme.Color,
    decoration : dicomheaderview.theme.Decoration,
    font : dicomheaderview.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : dicomheaderview.theme.Appearance
  }
});