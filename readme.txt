DICOM Header Viewer
===================

Displays the header information for a given list of sessions in a table and the master list of DICOM tags next to it.
